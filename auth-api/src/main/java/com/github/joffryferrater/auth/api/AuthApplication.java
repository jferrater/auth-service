package com.github.joffryferrater.auth.api;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by joffer on 12/7/2017
 */
@ApplicationPath("/")
public class AuthApplication extends Application {

}
