package com.github.joffryferrater.auth.lib.jwt;

import com.github.joffryferrater.auth.lib.Payload;
import com.github.joffryferrater.auth.lib.TokenUtility;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWTClaimsSet;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by joffer on 12/2/2017
 */
public class JsonWebTokenBuilderTest {

    private JsonWebTokenBuilder jsonWebTokenBuilder;

    @Before
    public void setUp() throws Exception {
        jsonWebTokenBuilder = new JsonWebTokenBuilder();
    }

    @Test
    public void testEncryptedCreateToken() throws JOSEException, ParseException {
        Payload payload = new Payload();
        payload.setAudiences(Arrays.asList("audience"));
        payload.setIssuer("Axiomatics");
        final String jti = UUID.randomUUID().toString();
        payload.setJti(jti);
        payload.setProjectId("projectId");
        payload.setSubject("subject");
        payload.setExpiryDate(new Date(new Date().getTime() + 60 * 60 * 1000));//60 min
        String token = jsonWebTokenBuilder.createEncryptedToken(payload);
        assertNotNull(token);

        JWTClaimsSet claimsSet = jsonWebTokenBuilder.decodeToken(token);
        assertNotNull(claimsSet);
        assertTrue(jsonWebTokenBuilder.verifyEncryptedToken(token));
        assertEquals("Axiomatics", TokenUtility.getIssuer(claimsSet));
        assertEquals(1, TokenUtility.getAudience(claimsSet).size());
        assertEquals(jti, TokenUtility.getJti(claimsSet));
        assertEquals("subject", TokenUtility.getSubject(claimsSet));
        assertEquals("projectId", TokenUtility.getProjectId(claimsSet));
    }

    @Test
    public void testRSASignedCreateToken() throws JOSEException, ParseException {
        Payload payload = new Payload();
        payload.setAudiences(Arrays.asList("audience"));
        payload.setIssuer("Axiomatics");
        final String jti = UUID.randomUUID().toString();
        payload.setJti(jti);
        payload.setProjectId("projectId");
        payload.setSubject("subject");
        payload.setExpiryDate(new Date(new Date().getTime() + 60 * 60 * 1000));//60 min

        String token = jsonWebTokenBuilder.createRSASignedToken(payload);
        assertNotNull(token);
        JWTClaimsSet claimsSet = jsonWebTokenBuilder.verifyRSASignature(token);
        assertNotNull(claimsSet);
        assertTrue(jsonWebTokenBuilder.verifyToken(token));
        assertEquals("Axiomatics", TokenUtility.getIssuer(claimsSet));
        assertEquals(1, TokenUtility.getAudience(claimsSet).size());
        assertEquals(jti, TokenUtility.getJti(claimsSet));
        assertEquals("subject", TokenUtility.getSubject(claimsSet));
        assertEquals("projectId", TokenUtility.getProjectId(claimsSet));
    }
}
