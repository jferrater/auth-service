package com.github.joffryferrater.auth.lib;

import org.junit.Before;
import org.junit.Test;

import java.security.KeyPair;

import static org.junit.Assert.assertNotNull;

/**
 * Created by joffer on 12/2/2017
 */
public class PKCS12KeyPairTest {

    private PKCS12KeyPair keyPair;

    @Before
    public void setUp() {
        keyPair = new PKCS12KeyPair();
    }

    @Test
    public void testKeystore() throws Exception {
        KeyPair keyPair = this.keyPair.getKeyPair();
        assertNotNull(keyPair);
        assertNotNull(keyPair.getPrivate());
        assertNotNull(keyPair.getPublic());
    }
}
