package com.github.joffryferrater.auth.lib;

/**
 * Created by joffer on 11/30/2017
 */
public class Header {

    private final String cookie;
    private final String projectId;

    public Header(String cookie, String projectId) {
        this.cookie = cookie;
        this.projectId = projectId;
    }

    public String getCookie() {
        return cookie;
    }

    public String getProjectId() {
        return projectId;
    }
}
