package com.github.joffryferrater.auth.lib;

import com.nimbusds.jwt.JWTClaimsSet;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by joffer on 12/2/2017
 */
public final class TokenUtility {

    private TokenUtility() {}

    public static String getIssuer(JWTClaimsSet claimsSet) {
        if(claimsSet == null) {
            return null;
        }
        return claimsSet.getIssuer();
    }

    public static List<String> getAudience(JWTClaimsSet claimsSet) {
        if(claimsSet == null) {
            return Collections.emptyList();
        }
        return claimsSet.getAudience();
    }

    public static String getSubject(JWTClaimsSet claimsSet) {
        if(claimsSet == null) {
            return null;
        }
        return claimsSet.getSubject();
    }

    public static String getJti(JWTClaimsSet claimsSet) {
        if(claimsSet == null) {
            return null;
        }
        return claimsSet.getJWTID();
    }

    public static Date getExpiryDate(JWTClaimsSet claimsSet) {
        if(claimsSet == null) {
            return null;
        }
        return claimsSet.getExpirationTime();
    }

    public static Date getIssueTime(JWTClaimsSet claimsSet) {
        if(claimsSet == null) {
            return null;
        }
        return claimsSet.getIssueTime();
    }

    public static Date getNotBeforeDate(JWTClaimsSet claimsSet) {
        if(claimsSet == null) {
            return null;
        }
        return claimsSet.getNotBeforeTime();
    }

    public static String getCommonName(JWTClaimsSet claimsSet) {
        if(claimsSet == null) {
            return null;
        }
        return (String) claimsSet.getClaim("cn");
    }

    public static String getProjectId(JWTClaimsSet claimsSet) {
        if(claimsSet == null) {
            return null;
        }
        return (String) claimsSet.getClaim("project");
    }
}
