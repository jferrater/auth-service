package com.github.joffryferrater.auth.lib.jwt;

import com.github.joffryferrater.auth.lib.PKCS12KeyPair;
import com.github.joffryferrater.auth.lib.Payload;
import com.github.joffryferrater.auth.lib.TokenUtility;
import com.github.joffryferrater.auth.lib.exception.InvalidSignatureException;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.EncryptedJWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import java.security.KeyPair;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.util.Date;

/**
 * Created by joffer on 11/30/2017
 */
public class JsonWebTokenBuilder {

    private KeyPair keyPair;

    public JsonWebTokenBuilder() throws Exception {
        //setting default keypair from self signed cert.
        this.keyPair = new PKCS12KeyPair().getKeyPair();
    }

    public JsonWebTokenBuilder(KeyPair keyPair) throws Exception {
        this();
        this.keyPair = keyPair;
    }

    private JWTClaimsSet buildClaimsSet(Payload payload) {
        return new JWTClaimsSet.Builder()
                .subject(payload.getSubject())
                .audience(payload.getAudiences())
                .expirationTime(payload.getExpiryDate())
                .issuer(payload.getIssuer())
                .jwtID(payload.getJti())
                .issueTime(new Date())
                .notBeforeTime(payload.getNotBeforeDate())
                .claim("project", payload.getProjectId())
                .build();
    }

    private JWEHeader buildHeader() {
        return new JWEHeader(JWEAlgorithm.RSA_OAEP_256, EncryptionMethod.A128GCM);
    }

    /**
     * Create encrypted token.
     *
     * @param payload The token payload.
     * @return The token.
     * @throws JOSEException
     */
    public String createEncryptedToken(Payload payload) throws JOSEException {
        EncryptedJWT encryptedJWT = new EncryptedJWT(buildHeader(), buildClaimsSet(payload));
        RSAEncrypter rsaEncrypter = new RSAEncrypter((RSAPublicKey) keyPair.getPublic());
        encryptedJWT.encrypt(rsaEncrypter);
        return encryptedJWT.serialize();
    }

    /**
     * Create RSA signed token.
     * @param payload, The token payload.
     * @return The token
     * @throws JOSEException
     */
    public String createRSASignedToken(Payload payload) throws JOSEException {
        JWSSigner signer = new RSASSASigner(keyPair.getPrivate());
        JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256)
                .keyID(payload.getProjectId()).build();
        SignedJWT signedJWT = new SignedJWT(header,
                buildClaimsSet(payload));
        signedJWT.sign(signer);
        return signedJWT.serialize();
    }

    JWTClaimsSet verifyRSASignature(String token) throws ParseException, JOSEException {
        SignedJWT signedJWT = SignedJWT.parse(token);
        JWSVerifier verifier = new RSASSAVerifier((RSAPublicKey) keyPair.getPublic());
        if(signedJWT.verify(verifier)){
            return signedJWT.getJWTClaimsSet();
        }
        throw new InvalidSignatureException("Invalid token signature");
    }

    /**
     * Verify token signature and expiry.
     *
     * @param token, The token.
     *
     * @return true if token is valid, otherwise false.
     * @throws JOSEException
     * @throws ParseException
     */
    public boolean verifyToken(String token) throws JOSEException, ParseException {
        JWTClaimsSet claimsSet = verifyRSASignature(token);
        if(claimsSet == null) {
            return false;
        }
        return new Date().before(claimsSet.getExpirationTime()) ? true : false;
    }

    JWTClaimsSet decodeToken(String token) throws ParseException, JOSEException {
        EncryptedJWT encryptedJWT = EncryptedJWT.parse(token);
        encryptedJWT.decrypt(new RSADecrypter(keyPair.getPrivate()));
        return encryptedJWT.getJWTClaimsSet();
    }

    public boolean verifyEncryptedToken(String token) throws ParseException, JOSEException {
        JWTClaimsSet claimsSet = decodeToken(token);
        if(claimsSet == null) {
            return false;
        }
        return new Date().before(claimsSet.getExpirationTime()) ? true : false;
    }

    public KeyPair getKeyPair() {
        return keyPair;
    }

    public void setKeyPair(KeyPair keyPair) {
        this.keyPair = keyPair;
    }
}
