package com.github.joffryferrater.auth.lib.exception;

/**
 * Created by joffer on 12/2/2017
 */
public class InvalidSignatureException extends RuntimeException {

    public InvalidSignatureException(String message) {
        super(message);
    }

}
