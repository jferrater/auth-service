package com.github.joffryferrater.auth.lib.trustmanager;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by joffer on 12/2/2017
 */
public class DefaultTrustManager implements X509TrustManager {

    private final X509TrustManager javaTrustManager;
    private final X509TrustManager addedTrustManager;

    public DefaultTrustManager(KeyStore trustStore) throws KeyStoreException, NoSuchAlgorithmException {

        if (trustStore == null) {
            throw new IllegalArgumentException("Missing trustoStore");
        }
        this.javaTrustManager = getTrustManager(null);
        this.addedTrustManager = getTrustManager(trustStore);
    }

    @Override
    public void checkClientTrusted(X509Certificate[] x509Certificates, String authType) throws CertificateException {
        try {
            javaTrustManager.checkClientTrusted(x509Certificates, authType);
        } catch (CertificateException e) {
            addedTrustManager.checkClientTrusted(x509Certificates, authType);
        }
    }

    @Override
    public void checkServerTrusted(X509Certificate[] x509Certificates, String authType) throws CertificateException {
        try {
            addedTrustManager.checkServerTrusted(x509Certificates, authType);
        } catch (CertificateException e) {
            javaTrustManager.checkServerTrusted(x509Certificates, authType);
        }
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        List<X509Certificate> issuers = new ArrayList<>();
        issuers.addAll(Arrays.asList(addedTrustManager.getAcceptedIssuers()));
        issuers.addAll(Arrays.asList(javaTrustManager.getAcceptedIssuers()));
        return issuers.toArray(new X509Certificate[issuers.size()]);
    }

    private X509TrustManager getTrustManager(KeyStore trustStore) throws NoSuchAlgorithmException, KeyStoreException {
        String defaultAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(defaultAlgorithm);
        trustManagerFactory.init(trustStore);
        TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
        for (TrustManager tm : trustManagers) {
            if (tm instanceof X509TrustManager) {
                return (X509TrustManager) tm;
            }
        }
        throw new IllegalStateException("Missed X509TrustManager in "
                + Arrays.toString(trustManagers));
    }
}
