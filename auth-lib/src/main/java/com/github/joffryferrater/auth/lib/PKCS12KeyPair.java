package com.github.joffryferrater.auth.lib;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;

/**
 * Created by joffer on 11/30/2017
 */
public class PKCS12KeyPair {

    private String keystoreFile = "C:\\Users\\joffer\\Documents\\Axiomatics\\Repos\\auth-service\\auth-service\\auth-lib\\src\\main\\resources\\keystore.12";
    private static final String ALIAS = "axiomatics";

    public PKCS12KeyPair() {
        setFilePath(keystoreFile);
    }

    public PKCS12KeyPair(String keystoreFile) {
        this();
        this.keystoreFile = keystoreFile;
    }


    public KeyPair getKeyPair() throws Exception {
        InputStream is = null;
        try {
            is = getFileInputStream(keystoreFile);

            KeyStore keystore = KeyStore.getInstance("PKCS12");
            keystore.load(is, "".toCharArray());

            Key key = keystore.getKey(ALIAS, "".toCharArray());
            if (key instanceof PrivateKey) {
                // Get certificate of public key
                Certificate cert = keystore.getCertificate(ALIAS);

                // Get public key
                PublicKey publicKey = cert.getPublicKey();

                // Return a key pair
                return new KeyPair(publicKey, (PrivateKey) key);
            }
        } finally {
            if(is != null) {
                is.close();
            }
        }
        throw new IllegalArgumentException("Invalid pkcs12 file");
    }

    private InputStream getFileInputStream(String keystoreFileDir) throws FileNotFoundException {
        File keystoreFile = new File(keystoreFileDir);
        return new FileInputStream(keystoreFile);
    }

    void setFilePath(String filePath) {
        this.keystoreFile = filePath;
    }

    String getFilePath() {
        return keystoreFile;
    }
}
